---
title: "Welcome!"
weight: 1
chapter: true
---

# Welcome!

We're happy to have you! This website will help guide you to become an Inkscape Contributor.

Here you will find important information, guides and processes of the Inkscape project.

As a new (or old) Contributor, you are welcome to amend the guide with information that you would have liked to know earlier.

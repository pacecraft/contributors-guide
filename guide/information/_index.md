---
title: Information about the Project
chapter: true
weight: 2
---

# Project Fundamentals

Inkscape is a free and Open Source Vector graphics editor - a computer program; and Inkscape is also a worldwide community of contributors who work together to create and promote the software 'Inkscape'.

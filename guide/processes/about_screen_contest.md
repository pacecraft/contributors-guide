---
title: Running an About Screen Contest
weight: 2
---

## Goals

Finding a new About Screen artwork for a new Inkscape version, and:

* encouraging testing the new version
* promoting Inkscape in general and the new version with its features
* generating artwork that can be used in other areas of the project

## Teams involved
{{<mermaid align="left">}}
%%{init: {'theme':'forest', 'flowchart':{'nodeSpacing': -20}, 'themeVariables': {'fontSize': '30px'}}}%%
flowchart TD
    id1[/Vectors/]
    id2[/Website/]
    id3[/Developers/]
{{</mermaid>}}

## Timeline

…

## Tasks by team

…

---
title: Meetings
weight: 1
---

* [ ] Announce meeting
* [ ] Hold meeting
* [ ] Create meeting notes
* [ ] Publish meeting notes
* [ ] Announce published meeting notes

---
title: Release
menuTitle: "Releasing a new Inkscape version"
weight: 3
---

## Teams involved

{{<mermaid align="left">}}
%%{init: {'theme':'forest', 'flowchart':{'nodeSpacing': -20}, 'themeVariables': {'fontSize': '30px'}}}%%
flowchart TD
    id1[/Vectors/]
    id2[/Website/]
    id3[/Developers/]
    id4[/Packagers/]
    id5[/Testers/]
    id6[/Documentation/]
    id7[/Translators/]
{{</mermaid>}}

## Goals

Release a productively usable program for all three major desktop operating systems with:

* New artwork for About Screen (major releases only)
* Blocker bugs fixed
* Translations done
* Documentation up-to-date
* Download files available
* Promotion for new version

## Timeline

* Open development
* Feature freeze
* About Screen Contest
* String freeze
* Release

## Tasks by team

### Release manager

- [ ] Coordinates teams
- [ ] Announces feature freeze to developers
- [ ] Announces string freeze to translators
- [ ] Makes sure that all parts of the program are up-to-date at the time of the release:
    - all mergeable 'backport-proposed' MRs merged
    - all pending and mergeable translation MRs merged
    - version info and appdata up-to-date
    - all the various images in installers and the About screen up-to-date
    - all submodules up-to-date
    - man page and tutorial files up-to-date
- [ ] Tags versions in git
- [ ] Announces pre-, beta and actual release to packagers
- [ ] Publishes official signed source code package
- [ ] Sends official version announcement email

### Development

- [ ] Backport important bug fixes
- [ ] Fix blocker bugs identified by Testing team
- [ ] Check and merge translations
- [ ] Prepare codebase for release:
    - [ ] Swap images:
        - [ ] Windows installer
        - [ ] macOS installer
        - [ ] Welcome screen
        - [ ] About page
    - [ ] Update version number
- [ ] …

### Documentation

- [ ] Update tutorials
    - Update tutorial contents
    - Merge pending translation MRs
    - Check inkscape.org for whether website export worked
    - Make a merge request with generated tutorial files for Inkscape main repository
- [ ] Update man page
    - Update man page contents
    - Merge pending translation MRs
    - Check inkscape.org for whether website export worked
    - Make a merge request with generated tutorial files for Inkscape main repository
- [ ] Update keyboard shortcuts
    - Update shortcuts file contents
    - Merge pending translation MRs
    - Check inkscape.org for whether website export worked
- [ ] Write and publish release notes
    - Draft in Wiki
    - Create short version for releases app
    - Put long version into documentation export repository for use on website
    - Check inkscape.org for whether website export worked
- [ ] Adjust version numbers on Wiki main page and releases overview page as well as in releases insertable template

### Packagers

- [ ] Watch out for pre-releases, beta releases and release
- [ ] Use the alpha-/beta-/pre-releases as test runs
- [ ] Package Inkscape or prepare everything for your platform
- [ ] Make sure that all components of Inkscape are available and working in your package:
    - installer (if applicable)
    - extensions submodule with all necessary Python modules
    - extension manager submodule with all necessary Python modules
    - if dithering: custom lib2geom submodule
    - themes submodule
- [ ] Make new signed or at least checksum'ed Inkscape downloads available on the website:
    - Linux:
        - [ ] upload official AppImage
        - [ ] update official ppas
        - [ ] update official snaps
        - [ ] maintainers package Inkscape for their distro
    - Windows:
        - [ ] upload exe files (32/64bit)
        - [ ] upload 7zip files (32/64bit)
        - [ ] upload msi files(32/64bit)
        - [ ] update Windows Store and description
    - macOS:
        - [ ] upload dmg (Intel)
        - [ ] upload dmg (ARM)

### Testing

- [ ] Test continuously
- [ ] Manage bugs continuosly
- [ ] Identify blocker bugs and communicate them to dev team
- [ ] Prioritize / tag merge requests

### Translators

- [ ] Translate new strings in:
    - main program po file
    - Windows installer translation files
    - documentation repository
- [ ] Translate news article about release
- [ ] optional: Translate website releases app contents
- [ ] optional: Translate release notes
- [ ] Be responsive to questions about translation process and translation strings from other translators

### Vectors

- [ ] Organize About Screen Contest (see About Screen Contest guide page)
- [ ] Create promotional material for the release:
    - news article
    - social media posts
    - other: e.g. video, podcast, …
    - email for news outlets
- [ ] Organize release party

### Website

- [ ] Update front page images
- [ ] Setup release object for current release 
- [ ] Update image on releases page
- [ ] Teach contributors how to use the website features
- [ ] Be available for questions and fixing in case of failures
- [ ] Make sure available packages are included in the releases app and their descriptions are correct
- [ ] Switch branch for 'stable development branch' release, if necessary and adjust descriptions
- [ ] optional: Make sure release uploads are named and described nicely in the website gallery



{{< quizdown >}}

---
primary_color: steelblue
secondary_color: '#e8e8e8'
text_color: black
shuffle_questions: false
shuffle_answers: true
locale: en
---

# Release files

Where are the files for a new Inkscape version uploaded?

> Think of where you downloaded yours!

1. [x] https://inkscape.org/release
1. [ ] https://gitlab.com/inkscape/inkscape
1. [ ] https://chip.de
1. [ ] https://megaupload.nz


# Translation

How do you contact translators for letting them know about a release being ready for translation?

> Are you _subscribed_ yet?

1. [ ] via IRC
1. [x] via Translators' mailing list
1. [ ] via rocket.chat
1. [ ] via phone


# Release order

Please bring the following into sequence!

> Think logically!

1. Develop new features
2. Translate new translatable phrases
3. Tell packagers about a new release secretly
4. Upload release files
5. Publish release news

{{< /quizdown >}}
